﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TugasDay7.Models;
using System.Collections.Generic;

namespace TugasDay7.Controllers
{
    [ApiController]
    public class GetController : ControllerBase
    {
        private List<ItemUsers> _itemUsers;
        private List<ItemTasks> _itemTasks;

        public GetController()
        {
            _itemUsers = new List<ItemUsers>();
            _itemTasks = new List<ItemTasks>();
        }

        [HttpGet]
        [Route("api/tasks/GetUserWithTask")]
        public ActionResult<List<UserWithTaskModel>> GetUserWithTask()
        {
            List<UserWithTaskModel> userWithTasks = new List<UserWithTaskModel>();

            foreach (var user in _itemUsers)
            {
                var tasks = _itemTasks.FindAll(t => t.fk_users_id == user.pk_users_id);
                userWithTasks.Add(new UserWithTaskModel { User = user, Tasks = tasks });
            }

            return userWithTasks;
        }
    }
}
