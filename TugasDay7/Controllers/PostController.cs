﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TugasDay7.Models;
using System.Collections.Generic;

namespace TugasDay7.Controllers
{
    [ApiController]
    public class PostController : ControllerBase
    {
        private List<ItemUsers> _itemUsers;
        private List<ItemTasks> _itemTasks;

        public PostController()
        {
            _itemUsers = new List<ItemUsers>();
            _itemTasks = new List<ItemTasks>();
        }

        [HttpPost]
        [Route("api/tasks/AddUserWithTask")]
        public ActionResult<AddUserWithTaskModel> AddUserWithTask(AddUserWithTaskModel inputData)
        {

            var user = new ItemUsers { name = inputData.name };
            _itemUsers.Add(user);

            foreach (var task in inputData.tasks)
            {
                var newTask = new ItemTasks { task_detail = task.task_detail };
                newTask.fk_users_id = user.pk_users_id;
                _itemTasks.Add(newTask);
            }

            return inputData;
        }
    }
}
