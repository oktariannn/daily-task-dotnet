﻿namespace TugasDay7.Models
{
    public class AddUserWithTaskModel
    {
        public string name { get; set; }
        public List<ItemTasks> tasks { get; set; }
    }
}
