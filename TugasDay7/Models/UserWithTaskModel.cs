﻿namespace TugasDay7.Models
{
    public class UserWithTaskModel
    {
        public ItemUsers User { get; set; }
        public List<ItemTasks> Tasks { get; set; }
    }
}
